package ru.tsc.avramenko.tm.command.project;

import ru.tsc.avramenko.tm.command.AbstractProjectCommand;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class ProjectFinishByIndexCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-finish-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish project by index.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().finishByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectService().finishByIndex(index);
        System.out.println("[OK]");
    }

}